
# invoke sed to edit file
# sed script to remove blank lines
# Targeted file to run script against

# ^ focus star in the line and $ end of line

sed -i -r '/^\s*$/d' access-logs


# try find something begin in line

grep '^Graham' customer-data.txt

# try find end of lines /nologin information. USE passwd-file
grep 'nologin$' /passwd

######################################
# targeting plank lines
sed -i -r '/^\s*$/d' access-logs 


