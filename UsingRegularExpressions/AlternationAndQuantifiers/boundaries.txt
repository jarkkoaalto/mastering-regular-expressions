# Boundaries ensure that Van is not of another word while searching text

# GREP use to search a file
# regular expression for the name Van
# customer-data is targeted file to search against

grep '\bVan\b' customer-data.txt
# or you can search litlse wider
grep '\bVan\B' customer-data.txt
