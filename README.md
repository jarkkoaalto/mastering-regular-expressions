# Mastering Regular Expressions #

OVERVIEW
In Mastering Regular Expressions, we begin by breaking dowm the "vocabulary" and "grammar" of regular expressions: First by learning just what all those symbols and shortcuts mean, then by learning how to manipulate those tokens to craft complex regular expressions that achieve just the right desired result. We'll do this by using come common Linux tools, such as grep and sed, as well as through Perl and, eventually, JavaScript for some front-end regex use examples.


The second part of this course involved three different projects demonstrating ways to combine the use of regular expressions with various scripts and scripting languages to get the desired matches and results.


Course Content

###### Using Regular Expressions
###### Alternation and Quantifiers
###### Classes and Groups
###### Lookarounds
###### Conditionals
###### Projects

#### sed
#### Perl
#### JavaScript