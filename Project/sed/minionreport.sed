#!/usr/bin/sed
# Using a sed script to generate human-readable files part - 1 
# usind hs.json fil# Using a sed script to generate human-readable files part - # 1  usind hs.json filee

# remove file spacing using command::: sed -r 's/^\s+//' hs.test.json
# if you want run minion.sed you have use dommand:: sed -E -f minionreport.sed hs.test.json
s/^\s+//
1s/(.+?):/RUN REPORT: \1\n/ 
#  Remove dash lines
/-{10}/d
# remove deginning into the line remove _ lines_ 
/^[a-z]+_\|/d
# removete changes and pchanges
/p?changes:/d
/^[a-z_]+:/ {
N
s/\n\s+/ /
}
# remove lines what we don't need. We dont need run_num
/^(__run_num__|duration|named|result|stat_time|__start_ran__)/d
s/__id__/ID/
s/__sls__/SLS/
s/comment: (.+)/Comment: \1\n/
